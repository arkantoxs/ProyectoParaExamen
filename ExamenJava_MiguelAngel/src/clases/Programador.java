/**
 * 
 */
package clases;
import clases.Empleado;
/**
 * @author Miguel Angel TQ
 *
 */
public class Programador extends Empleado{
	//Esta es la subclase Programador
	
	//ATRIBUTOS
	
	//Atributo lineasDeCodigoPorHora
	private int lineasDeCodigoPorHora;
		
	//Atributo Lengua Dominante
	private String lenguajeDominante;
	
	//CONSTRUCTORES
	
	//Constructor por defecto
	
	public Programador(){
		this.lineasDeCodigoPorHora=0;
		this.lenguajeDominante="";
	}
	
	//Constructor por parametros
	
	public Programador(int lineasDeCodigoPorHora,String lenguajeDominante){
		this.lineasDeCodigoPorHora=lineasDeCodigoPorHora;
		this.lenguajeDominante=lenguajeDominante;
	}
	//--------------------------------------------
	
	//GETS Y SETS

	public int getLineasDeCodigoPorHora() {
		return lineasDeCodigoPorHora;
	}

	public void setLineasDeCodigoPorHora(int lineasDeCodigoPorHora) {
		this.lineasDeCodigoPorHora = lineasDeCodigoPorHora;
	}

	public String getLenguajeDominante() {
		return lenguajeDominante;
	}

	public void setLenguajeDominante(String lenguajeDominante) {
		this.lenguajeDominante = lenguajeDominante;
	}
		
	//-----------------------------------------	

}
