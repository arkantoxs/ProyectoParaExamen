/**
 * 
 */
package clases;

import javax.swing.JOptionPane;

/**
 * @author Miguel Angel TQ
 *
 */
public class Empleado {
	//Esta es la super clase "Empleado"
	
	//ATRIBUTOS
	
	//Atributo nombre
	private String nombre;
	
	//Atributo cedula
	private String cedula;
	
	//Atributo edad
	private int edad;
	
	//Atributo casado tipo bool(true/false)
	private boolean casado;
	
	//Atributo salario
	private double salario;
	
	//CONSTRUCTORES---------------------------------------------
	
	//Constructor por defecto:
	public Empleado(){
		this.nombre="";
		this.cedula="";
		this.edad=0;
		this.casado=false;
		this.salario=0;
		
		
	}
	//---------------------------
	//Constructor por parametros:
	
	public Empleado(String nombre,String cedula,int edad,boolean casado, double salario){
		this.nombre=nombre;
		this.cedula=cedula;
		this.edad=edad;
		this.casado=casado;
		this.salario=salario;
		
	}
	//----------------------------
	
	
	
	
	//M�TODO CLASIFICACION EDAD------------------------------
	
	public void clasificacionEdad(){
		int edad=0;
		
		if(edad<=21){
			//JOptionPane.showMessageDialog(null, "Principiante");
			System.out.println("Principiante");
		}
		if(edad>=22 && edad<=35){
			//JOptionPane.showMessageDialog(null, "Intermedio");
			System.out.println("Intermedio");
		}
		if(edad>35){
			//JOptionPane.showMessageDialog(null, "Senior");
			System.out.println("Senior");
		}
		
	}
	//GETS Y SETS-----------------------------

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public boolean isCasado() {
		return casado;
	}

	public void setCasado(boolean casado) {
		this.casado = casado;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}
	//-------------------------------------------

}
