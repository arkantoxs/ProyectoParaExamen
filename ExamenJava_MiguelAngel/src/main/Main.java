/**
 * 
 */
package main;
import javax.swing.JOptionPane;


import clases.Empleado;
import clases.Programador;
/**
 * @author Miguel Angel TQ
 *
 */
public class Main {

	public static void main(String[] args) {
		
		
		/*String texto= JOptionPane.showInputDialog("Ingresa Tu edad");
		int edad = Integer.parseInt(texto);*/
		
		//OBJETOS
		
		//Objetos de la clase "empleado"---------------------------
		
		Empleado arrayEmpleados[]=new Empleado[2];
		
		arrayEmpleados[0]=new Empleado();
		arrayEmpleados[1]=new Empleado("Hector Alfonso Tautiva Parra","17348716",40,true,1800);
		arrayEmpleados[2]=new Empleado("Miguel Angel Tautiva Quintero","1125278787",21,false,1500);
		
		//Objetos de la clase "programador"
		Programador arrayProgramadores[] =new Programador[2]; 
		
		arrayProgramadores[0]=new Programador();
		arrayProgramadores[1]=new Programador(300,"Java");
		arrayProgramadores[2]=new Programador(250,"CSharp");
		
		//MOSTRAMOS EN PANTALLA
		System.out.println("EMPLEADOS\n");
		
		for (int i=0;i<arrayEmpleados.length;i++){
			 
			 System.out.println(arrayEmpleados[i]);
			}
		
		//POR ALGUNA RAZ�N EL ECLIPSE ME TIRA ERROR//FALTA MOSTRAR LOS DATOS

	}

}
